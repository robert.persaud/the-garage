import React, { Component } from 'react';
import Link from 'next/link';
import Layout from '../components/Layout';
import { observer, inject } from 'mobx-react';

@inject('store')
@observer
class Home extends Component<any, any> {
  

  // static async function you can add into any page in your app
  // ... fetch data and send them as props to our page
  static async getInitialProps({ mobxStore }: any) {
    await mobxStore.fetchVehicles();
    return {}; // resolves only store, but can add different properties here
  }

  constructor(props: any) {
    super(props);
  }

  // fetch data after client renders ...
  // componentDidMount() {
  //   this.props.store.fetchVehicles();
  // }


  render() {
    return (
      <Layout title="Home | SSR React/Next.js + GarageX">
        <div>
          <input type='button' onClick={this.props.store.addVehicle} value='add vehicle' />
          <p>Welcome to The Garage!</p>

          {/* styled-jsx better suited at component level */}          
          <p><Link href='/about'><a>About</a></Link></p>
          {/* <h3>{this.props.store.getVehiclesCount()}</h3> */}
          <ul>          
          {
            this.props.store.vehicles.map((i: { title: React.ReactNode; }) => <li>{i.title}</li>)            
          }          
          </ul>
        </div>
      </Layout>
    );
  }
}
  
export default Home;