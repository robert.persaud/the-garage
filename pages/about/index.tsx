
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import "./styles.scss";

@inject('store')
@observer
class About extends Component<any, any> {

  componentDidMount() {
    this.props.store.fetchVehicles();
  }
  render() {
    return (
    <div>
      <h1 className="about-foo">Hello, Robert</h1>      
    </div>
    );
  }
}

export default About;