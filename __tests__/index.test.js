/* eslint-env jest */

import { shallow } from 'enzyme';
import React from 'react';
import renderer from 'react-test-renderer';

import App from '../pages/index.tsx';

describe('With Enzyme', () => {
  xit('App shows "Hello world!"', () => {
    const app = shallow(<App />);
    expect(app.find('div').text()).toEqual('Welcome to The Garage!');
  });
});

describe('With Snapshot Testing', () => {
  xit('App shows "Welcome to The Garage!"', () => {
    const component = renderer.create(<App />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
