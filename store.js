import { action, observable, computed, flow } from 'mobx';
import { useStaticRendering } from 'mobx-react';
// import fetch from 'isomorphic-unfetch';
 


const isServer = !process.browser || typeof window === 'undefined';
useStaticRendering(isServer);

export default class Store {

    @observable
    _user = {};
    get user() {
        return this._user;
    }
    set user(value) {
        this._user = value;
    }
    @observable vehicles = []; 
    @observable state = 'pending'; // init, pending, done, error

    constructor(isServer, initialData = {}) {
        this.user = initialData.user != null ? initialData.user : {};
        this.vehicles = initialData.vehicles != null ? initialData.vehicles : [];
        this.state = initialData.state != null ? initialData.state : 'init';
    }
    
    fetchVehicles = flow(function * () {
        this.vehicles = [];
        this.state = 'pending';
        try {
            const response = yield fetch('https://jsonplaceholder.typicode.com/posts');
            const data = yield response.json();
            this.state = 'done';
            this.vehicles = data;
        } catch(error) {
            console.log('fetchVehicles ->', error);
            this.state = 'error';
        }
    }).bind(this);

    postData() {
        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            headers: { 'Content-type': 'application/json; charset=UTF-8' },
            body: JSON.stringify({
                title: '2017 Honda Civic',
                body: {
                    styleId: 390509,
                    trim: "Base",
                    vehicleId: "0d0bada20a0a00f97867b4f2034abbc1",
                    vin: "JN1AZ4EH2JM570613",
                    year: 2018
                },
                userId: Math.random()
            })
        });
    }

    addVehicle = flow(function * () {
        try {
            const response = yield fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                headers: { 'Content-type': 'application/json; charset=UTF-8' },
                body: JSON.stringify({
                    title: '2017 Honda Civic',
                    body: {
                        styleId: 390509,
                        trim: "Base",
                        vehicleId: "0d0bada20a0a00f97867b4f2034abbc1",
                        vin: "JN1AZ4EH2JM570613",
                        year: 2018
                    },
                    userId: Math.random()
                })
            });
            const vehicle = yield response.data;
            console.log('addVehicle|response ->', response, vehicle);            
            this.vehicles.push(vehicle);
        } catch (error) {
            console.log('addVehicle|error->', error);
        }
    }).bind(this);

    @computed get vehiclesCount() {
        return this.vehicles.length;
    }
}

let store = null;

export function initializeStore(initialData) {
  if (isServer) {
    return new Store(isServer, initialData);
  }
  if (store === null) {
    store = new Store(isServer, initialData);
  }

  return store;
}