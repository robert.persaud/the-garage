const withPlugins = require('next-compose-plugins');
const withTypescript = require('@zeit/next-typescript');
const withSass = require('@zeit/next-sass');

const nextConfig = {
    target: "serverless"
};

module.exports = withPlugins([
    [withSass],
    [withTypescript]
], nextConfig);
